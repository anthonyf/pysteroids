#!/usr/bin/python
import pygame, sys
from pygame.locals import *
from random import randint, randrange
from time import time

fpsClock = pygame.time.Clock()

pygame.init()

SCREEN_WIDTH = 1024
SCREEN_HEIGHT = 768
surface = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
pygame.display.set_caption('Pysteroids')

class StarField(pygame.sprite.Sprite):    
    def __init__(self, width = SCREEN_WIDTH, height = SCREEN_HEIGHT, num_of_stars = 1000):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.Surface((width, height))
        self.image.fill(pygame.Color(0, 0, 0))
        self.rect = Rect(0,0,width,height)
        points = [(randint(0, width), randint(0, height)) for x in range(num_of_stars)] 
        color = pygame.Color(255,255,255)
        for point in points:
            self.image.set_at(point, color)

def vector2xy(direction, magnitude):
    from math import sin, cos, radians, pi
    direction = radians(direction) - (pi/2)
    x = magnitude * cos(direction)
    y = magnitude * sin(direction)
    return x, y

class GameObject(pygame.sprite.Sprite):
    def __init__(self, original_image):
        pygame.sprite.Sprite.__init__(self)
        self.direction = 0
        self.image = original_image
        self.motionx, self.motiony = 0, 0
        self.original_image = original_image
        self.rect = Rect(0,0,self.image.get_width(),self.image.get_height())

    def update(self):        
        posx = (self.rect.centerx + self.motionx) % SCREEN_WIDTH
        posy = (self.rect.centery + self.motiony) % SCREEN_HEIGHT
        self.image = pygame.transform.rotate(self.original_image, -self.direction)
        self.rect.width = self.image.get_width()
        self.rect.height = self.image.get_height()
        self.rect.centerx = posx
        self.rect.centery = posy
            
class Ship(GameObject):
    image = pygame.image.load("images/i_are_spaceship.png")
    thrust_image = image.subsurface(Rect(0,0,15,24))
    idle_image = image.subsurface(Rect(16,0,15,24))
    thrust_image = pygame.transform.scale2x(thrust_image)
    idle_image = pygame.transform.scale2x(idle_image)
    def __init__(self):
        GameObject.__init__(self, Ship.idle_image)
        self.rect.center = (SCREEN_WIDTH/2,SCREEN_HEIGHT/2)

    def rotate_left(self):
        self.direction -= 10

    def rotate_right(self):
        self.direction += 10

    def idle(self):
        self.original_image = Ship.idle_image

    def accelerate(self):
        self.original_image = Ship.thrust_image
        x, y = vector2xy(self.direction, 1)
        self.motionx += x
        self.motiony += y

class Bullet(GameObject):
    image = pygame.image.load("images/i_are_spaceship.png")
    image = image.subsurface(Rect(34,17,3,7))
    image = pygame.transform.scale(image, (12, 28))
    sound = pygame.mixer.Sound("images/shot.wav")

    def __init__(self, ship):
        GameObject.__init__(self, Bullet.image)
        self.direction = ship.direction
        self.rect.center = ship.rect.center
        self.motionx, self.motiony = vector2xy(self.direction, 10)
        self.motionx += ship.motionx
        self.motiony += ship.motiony
        Bullet.sound.play()
        self.created_time = time()

    def update(self):
        GameObject.update(self)
        if time() - self.created_time > 5:
            self.kill()

class Asteroid(GameObject):
    source = pygame.image.load("images/1346943991.png")
    images = []
    images.append(pygame.transform.smoothscale(source, (50, 50)))
    images.append(pygame.transform.smoothscale(source, (100, 100)))
    images.append(pygame.transform.smoothscale(source, (200, 200)))
    def __init__(self, size, parent = None, dir = None):
        self.size = size
        GameObject.__init__(self, Asteroid.images[self.size-1])
        if parent is None:
            self.rect.centerx = randint(0, SCREEN_WIDTH)
            self.rect.centery = randint(0, SCREEN_HEIGHT)
        else:
            self.rect.center = parent.rect.center
        self.direction = dir or randint(0, 360)
        self.rotation_step = randrange(-2, 2, 1)
        if parent is None:
            self.motionx, self.motiony = vector2xy(randint(0, 360), randint(1, 3))
        else:
            self.motionx, self.motiony = vector2xy(dir, 4)

    def update(self):
        self.direction += self.rotation_step
        GameObject.update(self)
        for bullet in bullets:
            if pygame.sprite.collide_mask(bullet, self):
                self.kill()
                bullet.kill()
                if self.size > 1:
                    # bust the asteroid into 4 pieces
                    random_dir = randint(0, 360)
                    for x in range(4):
                        dir = random_dir + x * 90
                        asteroid = Asteroid(self.size-1, self, dir)
                        asteroids.add(asteroid)
                        sprites.add(asteroid)
                sprites.add(Explosion(self.rect.center))

class Explosion(GameObject):
    sound = pygame.mixer.Sound("images/explosion.wav")
    source = pygame.image.load("images/i_are_spaceship.png")
    images = []
    for x in range(4):
        image = source.subsurface(Rect(x*16,32,15,15))
        images.append(pygame.transform.scale(image, (100,100)))

    def __init__(self, position):
        GameObject.__init__(self, Explosion.images[0])
        self.start_time = time()
        self.rect = Rect(0,0,15,15)
        self.rect.center = position
        Explosion.sound.play()

    def update(self):
        GameObject.update(self)
        elapsed_time = time() - self.start_time
        if elapsed_time > .6:
            self.kill()
        elif elapsed_time > .5:
            self.image = Explosion.images[3]
        elif elapsed_time > .4:
            self.image = Explosion.images[2]
        elif elapsed_time > .3:
            self.image = Explosion.images[1]
        elif elapsed_time > .2:
            self.image = Explosion.images[0]
        elif elapsed_time > .1:
            self.image = Explosion.images[1]
        else:
            self.image = Explosion.images[2]
            
            
        

ship = Ship()
sprites = pygame.sprite.OrderedUpdates()
bullets = pygame.sprite.Group()
asteroids = pygame.sprite.Group()
sprites.add(StarField())
sprites.add(ship)

for x in range(3):
    asteroid = Asteroid(3)
    asteroids.add(asteroid)
    sprites.add(asteroid)

# game loop
while True:
    # draw game objects
    sprites.draw(surface)

    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
        elif event.type == KEYDOWN:
            if event.key == K_ESCAPE:
                pygame.event.post(pygame.event.Event(QUIT))
            elif event.key == K_SPACE:
                bullet = Bullet(ship)
                sprites.add(bullet)
                bullets.add(bullet)

    # handle ship rotation
    keystate = pygame.key.get_pressed()
    ship.idle()
    if keystate[K_LEFT]:
        ship.rotate_left()
    elif keystate[K_RIGHT]:
        ship.rotate_right()
    elif keystate[K_UP]:
        ship.accelerate()

    # update game objects
    sprites.update()

    pygame.display.update()
    fpsClock.tick(30)
